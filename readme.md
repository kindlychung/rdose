# Description

An R package for reading MaCH dosage files into R

# Install

    require(devtools)
    install_bitbucket("kindlychung/rdose")
